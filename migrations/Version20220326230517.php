<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220326230517 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hiit_program_exercice (hiit_program_id INT NOT NULL, exercice_id INT NOT NULL, INDEX IDX_A26B42E3E0B1D649 (hiit_program_id), INDEX IDX_A26B42E389D40298 (exercice_id), PRIMARY KEY(hiit_program_id, exercice_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hiit_program_exercice ADD CONSTRAINT FK_A26B42E3E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hiit_program_exercice ADD CONSTRAINT FK_A26B42E389D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE exercice_hiit_program');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exercice_hiit_program (exercice_id INT NOT NULL, hiit_program_id INT NOT NULL, INDEX IDX_D3D2AAB189D40298 (exercice_id), INDEX IDX_D3D2AAB1E0B1D649 (hiit_program_id), PRIMARY KEY(exercice_id, hiit_program_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE exercice_hiit_program ADD CONSTRAINT FK_D3D2AAB189D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exercice_hiit_program ADD CONSTRAINT FK_D3D2AAB1E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE hiit_program_exercice');
        $this->addSql('ALTER TABLE equipment CHANGE equipment_name equipment_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipment_img equipment_img VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE exercice CHANGE exercice_name exercice_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_desc exercice_desc LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_link exercice_link LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_img exercice_img VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE hiit_program CHANGE hiit_name hiit_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE muscle CHANGE muscle_name muscle_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE muscle_group CHANGE mg_name mg_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE username username VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE nom nom VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
