<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220322132745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE equipment (id INT AUTO_INCREMENT NOT NULL, equipment_name VARCHAR(255) NOT NULL, equipment_img VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment_user (equipment_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_B717074F517FE9FE (equipment_id), INDEX IDX_B717074FA76ED395 (user_id), PRIMARY KEY(equipment_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exercice (id INT AUTO_INCREMENT NOT NULL, first_muscle_id_id INT NOT NULL, second_muscle_id_id INT DEFAULT NULL, third_muscle_id_id INT DEFAULT NULL, required_equipment_id_id INT DEFAULT NULL, exercice_name VARCHAR(255) NOT NULL, cardio_focus TINYINT(1) NOT NULL, cardio_intensity SMALLINT NOT NULL, first_muscle_intensity SMALLINT NOT NULL, second_muscle_intensity SMALLINT DEFAULT NULL, third_muscle_intensity SMALLINT DEFAULT NULL, exercice_desc LONGTEXT DEFAULT NULL, exercice_link LONGTEXT DEFAULT NULL, exercice_img VARCHAR(255) DEFAULT NULL, INDEX IDX_E418C74D554D858D (first_muscle_id_id), INDEX IDX_E418C74DDADF0BE3 (second_muscle_id_id), INDEX IDX_E418C74D9A298BF2 (third_muscle_id_id), INDEX IDX_E418C74DC7183105 (required_equipment_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exercice_hiit_program (exercice_id INT NOT NULL, hiit_program_id INT NOT NULL, INDEX IDX_D3D2AAB189D40298 (exercice_id), INDEX IDX_D3D2AAB1E0B1D649 (hiit_program_id), PRIMARY KEY(exercice_id, hiit_program_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hiit_program (id INT AUTO_INCREMENT NOT NULL, creator_id_id INT NOT NULL, hiit_name VARCHAR(255) NOT NULL, timer INT NOT NULL, INDEX IDX_7D2880D7F05788E9 (creator_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hiit_program_user (hiit_program_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_AAF5A3C3E0B1D649 (hiit_program_id), INDEX IDX_AAF5A3C3A76ED395 (user_id), PRIMARY KEY(hiit_program_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE muscle (id INT AUTO_INCREMENT NOT NULL, muscle_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE muscle_muscle_group (muscle_id INT NOT NULL, muscle_group_id INT NOT NULL, INDEX IDX_E43DBAA7354FDBB4 (muscle_id), INDEX IDX_E43DBAA744004D0 (muscle_group_id), PRIMARY KEY(muscle_id, muscle_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE muscle_group (id INT AUTO_INCREMENT NOT NULL, mg_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, prenom VARCHAR(255) DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE equipment_user ADD CONSTRAINT FK_B717074F517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE equipment_user ADD CONSTRAINT FK_B717074FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exercice ADD CONSTRAINT FK_E418C74D554D858D FOREIGN KEY (first_muscle_id_id) REFERENCES muscle (id)');
        $this->addSql('ALTER TABLE exercice ADD CONSTRAINT FK_E418C74DDADF0BE3 FOREIGN KEY (second_muscle_id_id) REFERENCES muscle (id)');
        $this->addSql('ALTER TABLE exercice ADD CONSTRAINT FK_E418C74D9A298BF2 FOREIGN KEY (third_muscle_id_id) REFERENCES muscle (id)');
        $this->addSql('ALTER TABLE exercice ADD CONSTRAINT FK_E418C74DC7183105 FOREIGN KEY (required_equipment_id_id) REFERENCES equipment (id)');
        $this->addSql('ALTER TABLE exercice_hiit_program ADD CONSTRAINT FK_D3D2AAB189D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exercice_hiit_program ADD CONSTRAINT FK_D3D2AAB1E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hiit_program ADD CONSTRAINT FK_7D2880D7F05788E9 FOREIGN KEY (creator_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE hiit_program_user ADD CONSTRAINT FK_AAF5A3C3E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hiit_program_user ADD CONSTRAINT FK_AAF5A3C3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE muscle_muscle_group ADD CONSTRAINT FK_E43DBAA7354FDBB4 FOREIGN KEY (muscle_id) REFERENCES muscle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE muscle_muscle_group ADD CONSTRAINT FK_E43DBAA744004D0 FOREIGN KEY (muscle_group_id) REFERENCES muscle_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE equipment_user DROP FOREIGN KEY FK_B717074F517FE9FE');
        $this->addSql('ALTER TABLE exercice DROP FOREIGN KEY FK_E418C74DC7183105');
        $this->addSql('ALTER TABLE exercice_hiit_program DROP FOREIGN KEY FK_D3D2AAB189D40298');
        $this->addSql('ALTER TABLE exercice_hiit_program DROP FOREIGN KEY FK_D3D2AAB1E0B1D649');
        $this->addSql('ALTER TABLE hiit_program_user DROP FOREIGN KEY FK_AAF5A3C3E0B1D649');
        $this->addSql('ALTER TABLE exercice DROP FOREIGN KEY FK_E418C74D554D858D');
        $this->addSql('ALTER TABLE exercice DROP FOREIGN KEY FK_E418C74DDADF0BE3');
        $this->addSql('ALTER TABLE exercice DROP FOREIGN KEY FK_E418C74D9A298BF2');
        $this->addSql('ALTER TABLE muscle_muscle_group DROP FOREIGN KEY FK_E43DBAA7354FDBB4');
        $this->addSql('ALTER TABLE muscle_muscle_group DROP FOREIGN KEY FK_E43DBAA744004D0');
        $this->addSql('ALTER TABLE equipment_user DROP FOREIGN KEY FK_B717074FA76ED395');
        $this->addSql('ALTER TABLE hiit_program DROP FOREIGN KEY FK_7D2880D7F05788E9');
        $this->addSql('ALTER TABLE hiit_program_user DROP FOREIGN KEY FK_AAF5A3C3A76ED395');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE equipment_user');
        $this->addSql('DROP TABLE exercice');
        $this->addSql('DROP TABLE exercice_hiit_program');
        $this->addSql('DROP TABLE hiit_program');
        $this->addSql('DROP TABLE hiit_program_user');
        $this->addSql('DROP TABLE muscle');
        $this->addSql('DROP TABLE muscle_muscle_group');
        $this->addSql('DROP TABLE muscle_group');
        $this->addSql('DROP TABLE user');
    }
}
