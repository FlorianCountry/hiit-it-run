<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220326220040 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_hiit_program (user_id INT NOT NULL, hiit_program_id INT NOT NULL, INDEX IDX_FDE4FCD5A76ED395 (user_id), INDEX IDX_FDE4FCD5E0B1D649 (hiit_program_id), PRIMARY KEY(user_id, hiit_program_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_hiit_program ADD CONSTRAINT FK_FDE4FCD5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_hiit_program ADD CONSTRAINT FK_FDE4FCD5E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE hiit_program_user');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hiit_program_user (hiit_program_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_AAF5A3C3E0B1D649 (hiit_program_id), INDEX IDX_AAF5A3C3A76ED395 (user_id), PRIMARY KEY(hiit_program_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE hiit_program_user ADD CONSTRAINT FK_AAF5A3C3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hiit_program_user ADD CONSTRAINT FK_AAF5A3C3E0B1D649 FOREIGN KEY (hiit_program_id) REFERENCES hiit_program (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_hiit_program');
        $this->addSql('ALTER TABLE equipment CHANGE equipment_name equipment_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipment_img equipment_img VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE exercice CHANGE exercice_name exercice_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_desc exercice_desc LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_link exercice_link LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE exercice_img exercice_img VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE hiit_program CHANGE hiit_name hiit_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE muscle CHANGE muscle_name muscle_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE muscle_group CHANGE mg_name mg_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE username username VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE nom nom VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
