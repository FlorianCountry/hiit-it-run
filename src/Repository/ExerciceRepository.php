<?php

namespace App\Repository;

use App\Data\HiitGenerator;
use App\Entity\Exercice;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exercice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exercice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exercice[]    findAll()
 * @method Exercice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExerciceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exercice::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Exercice $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Exercice $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Récupérer des exercices randomisés en fonction des groupes musculaires choisis en mode déconnecté (pas de matériel)
     * @return array
     */
    public function getRandomExercicesByMuscleGroupNoEquipment($mgId, $nbExercices):array{
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.firstMuscleId', 'm' )
            ->leftJoin('m.muscleGroupId', 'mg')
            ->leftJoin('e.secondMuscleId', 'ms' )
            ->leftJoin('ms.muscleGroupId', 'mgs')
            ->leftJoin('e.thirdMuscleId', 'mt' )
            ->leftJoin('mt.muscleGroupId', 'mgt')
            ->Where('mg.id = :mgId')
            ->orWhere('mgs.id = :mgId')
            ->orWhere('mgt.id = :mgId')
            ->andWhere('e.requiredEquipmentId is NULL')
            ->orderBy('RAND()')
            ->distinct()
            ->setParameter('mgId', $mgId)
            ->setMaxResults($nbExercices);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    /**
     * Récupérer des exercices randomisés en fonction des groupes musculaires choisis en mode connecté
     * @return array
     */
    public function getRandomExercicesByMuscleGroup($mgId, $nbExercices, User $user):array{
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.firstMuscleId', 'm' )
            ->leftJoin('m.muscleGroupId', 'mg')
            ->leftJoin('e.secondMuscleId', 'ms' )
            ->leftJoin('ms.muscleGroupId', 'mgs')
            ->leftJoin('e.thirdMuscleId', 'mt' )
            ->leftJoin('mt.muscleGroupId', 'mgt')
            ->leftJoin('e.requiredEquipmentId', 'eq')
            ->leftJoin('eq.userId', 'u')
            ->Where('mg.id = :mgId')
            ->orWhere('mgs.id = :mgId')
            ->orWhere('mgt.id = :mgId')
            ->andWhere('e.requiredEquipmentId IN (:equipment) OR e.requiredEquipmentId is NULL')
            ->orderBy('RAND()')
            ->distinct()
            ->setParameter('mgId', $mgId)
            ->setParameter('equipment', $user->getEquipment())
            ->setMaxResults($nbExercices);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function getRandomExercicesByMuscle($muscleId, $nbExercices):array{
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.firstMuscleId', 'm')
            ->leftJoin('e.secondMuscleId', 'ms' )
            ->leftJoin('e.thirdMuscleId', 'mt' )
            ->Where('m.id = :muscleId')
            ->orWhere('ms.id = :muscleId')
            ->orWhere('mt.id = :muscleId')
            ->orderBy('RAND()')
            ->distinct()
            ->setParameter('muscleId', $muscleId)
            ->setMaxResults($nbExercices);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function getCardioFocusExercice($nbExercices, User $user):array{
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.requiredEquipmentId', 'eq')
            ->leftJoin('eq.userId', 'u')
            ->Where('e.cardioFocus = 1')
            ->andWhere('e.requiredEquipmentId IN (:equipment) OR e.requiredEquipmentId is NULL')
            ->orderBy('RAND()')
            ->distinct()
            ->setParameter('equipment', $user->getEquipment())
            ->setMaxResults($nbExercices);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function getCardioFocusExercicenoEquipment($nbExercices):array{
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.requiredEquipmentId', 'eq')
            ->leftJoin('eq.userId', 'u')
            ->Where('e.cardioFocus = 1')
            ->andWhere('e.requiredEquipmentId is NULL')
            ->orderBy('RAND()')
            ->distinct()
            ->setMaxResults($nbExercices);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    // /**
    //  * @return Exercice[] Returns an array of Exercice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exercice
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
