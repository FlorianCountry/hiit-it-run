<?php

namespace App\Repository;

use App\Entity\HiitProgram;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HiitProgram|null find($id, $lockMode = null, $lockVersion = null)
 * @method HiitProgram|null findOneBy(array $criteria, array $orderBy = null)
 * @method HiitProgram[]    findAll()
 * @method HiitProgram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HiitProgramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HiitProgram::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(HiitProgram $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(HiitProgram $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function countExercices(HiitProgram $hiitProgram)
    {
        $query = $this->createQueryBuilder('he')
            ->select('count(he.exercice_id)')
            ->from('hiit_program_exercice', 'he')
            ->setParameter('he.hiit_program_id', $hiitProgram->getId());
        return $query->getQuery()->getResult();

    }



    // /**
    //  * @return HiitProgram[] Returns an array of HiitProgram objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HiitProgram
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
