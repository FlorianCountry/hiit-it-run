<?php

namespace App\Entity;

use App\Repository\ExerciceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExerciceRepository::class)
 */
class Exercice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $exerciceName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cardioFocus;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cardioIntensity;

    /**
     * @ORM\Column(type="smallint")
     */
    private $firstMuscleIntensity;

    /**
     * @ORM\ManyToOne(targetEntity=Muscle::class, inversedBy="exercicesFirst")
     * @ORM\JoinColumn(nullable=false)
     */
    private $firstMuscleId;

    /**
     * @ORM\ManyToOne(targetEntity=Muscle::class, inversedBy="exercicesSecond")
     */
    private $secondMuscleId;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $secondMuscleIntensity;

    /**
     * @ORM\ManyToOne(targetEntity=Muscle::class, inversedBy="exercicesThird")
     */
    private $thirdMuscleId;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $thirdMuscleIntensity;

    /**
     * @ORM\ManyToOne(targetEntity=Equipment::class, inversedBy="exercices")
     */
    private $requiredEquipmentId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $exerciceDesc;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $exerciceLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $exerciceImg;

    /**
     * @ORM\ManyToMany(targetEntity=HiitProgram::class, mappedBy="exercices")
     */
    private $hiitId;

    public function __construct()
    {
        $this->hiitId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExerciceName(): ?string
    {
        return $this->exerciceName;
    }

    public function setExerciceName(string $exerciceName): self
    {
        $this->exerciceName = $exerciceName;

        return $this;
    }

    public function getCardioFocus(): ?bool
    {
        return $this->cardioFocus;
    }

    public function setCardioFocus(bool $cardioFocus): self
    {
        $this->cardioFocus = $cardioFocus;

        return $this;
    }

    public function getCardioIntensity(): ?int
    {
        return $this->cardioIntensity;
    }

    public function setCardioIntensity(int $cardioIntensity): self
    {
        $this->cardioIntensity = $cardioIntensity;

        return $this;
    }

    public function getFirstMuscleIntensity(): ?int
    {
        return $this->firstMuscleIntensity;
    }

    public function setFirstMuscleIntensity(int $firstMuscleIntensity): self
    {
        $this->firstMuscleIntensity = $firstMuscleIntensity;

        return $this;
    }

    public function getFirstMuscleId(): ?Muscle
    {
        return $this->firstMuscleId;
    }

    public function setFirstMuscleId(?Muscle $firstMuscleId): self
    {
        $this->firstMuscleId = $firstMuscleId;

        return $this;
    }

    public function getSecondMuscleId(): ?Muscle
    {
        return $this->secondMuscleId;
    }

    public function setSecondMuscleId(?Muscle $secondMuscleId): self
    {
        $this->secondMuscleId = $secondMuscleId;

        return $this;
    }

    public function getSecondMuscleIntensity(): ?int
    {
        return $this->secondMuscleIntensity;
    }

    public function setSecondMuscleIntensity(?int $secondMuscleIntensity): self
    {
        $this->secondMuscleIntensity = $secondMuscleIntensity;

        return $this;
    }

    public function getThirdMuscleId(): ?Muscle
    {
        return $this->thirdMuscleId;
    }

    public function setThirdMuscleId(?Muscle $thirdMuscleId): self
    {
        $this->thirdMuscleId = $thirdMuscleId;

        return $this;
    }

    public function getThirdMuscleIntensity(): ?int
    {
        return $this->thirdMuscleIntensity;
    }

    public function setThirdMuscleIntensity(?int $thirdMuscleIntensity): self
    {
        $this->thirdMuscleIntensity = $thirdMuscleIntensity;

        return $this;
    }

    public function getRequiredEquipmentId(): ?Equipment
    {
        return $this->requiredEquipmentId;
    }

    public function setRequiredEquipmentId(?Equipment $requiredEquipmentId): self
    {
        $this->requiredEquipmentId = $requiredEquipmentId;

        return $this;
    }

    public function getExerciceDesc(): ?string
    {
        return $this->exerciceDesc;
    }

    public function setExerciceDesc(?string $exerciceDesc): self
    {
        $this->exerciceDesc = $exerciceDesc;

        return $this;
    }

    public function getExerciceLink(): ?string
    {
        return $this->exerciceLink;
    }

    public function setExerciceLink(?string $exerciceLink): self
    {
        $this->exerciceLink = $exerciceLink;

        return $this;
    }

    public function getExerciceImg(): ?string
    {
        return $this->exerciceImg;
    }

    public function setExerciceImg(?string $exerciceImg): self
    {
        $this->exerciceImg = $exerciceImg;

        return $this;
    }

    /**
     * @return Collection<int, HiitProgram>
     */
    public function getHiitId(): Collection
    {
        return $this->hiitId;
    }

    public function addHiitId(HiitProgram $hiitId): self
    {
        if (!$this->hiitId->contains($hiitId)) {
            $this->hiitId[] = $hiitId;
            $hiitId->addExercice($this);
        }

        return $this;
    }

    public function removeHiitId(HiitProgram $hiitId): self
    {
        $this->hiitId->removeElement($hiitId);
        $hiitId->removeExercice($this);

        return $this;
    }
    public function __toString(){
        return $this->exerciceName;
    }
}
