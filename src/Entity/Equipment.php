<?php

namespace App\Entity;

use App\Repository\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 */
class Equipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equipmentName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $equipmentImg;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="equipment")
     */
    private $userId;

    /**
     * @ORM\OneToMany(targetEntity=Exercice::class, mappedBy="requiredEquipmentId")
     */
    private $exercices;

    public function __construct()
    {
        $this->userId = new ArrayCollection();
        $this->exercices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipmentName(): ?string
    {
        return $this->equipmentName;
    }

    public function setEquipmentName(string $equipmentName): self
    {
        $this->equipmentName = $equipmentName;

        return $this;
    }

    public function getEquipmentImg(): ?string
    {
        return $this->equipmentImg;
    }

    public function setEquipmentImg(?string $equipmentImg): self
    {
        $this->equipmentImg = $equipmentImg;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUserId(): Collection
    {
        return $this->userId;
    }

    public function addUserId(User $userId): self
    {
        if (!$this->userId->contains($userId)) {
            $this->userId[] = $userId;
            $userId->addEquipment($this);
        }

        return $this;
    }

    public function removeUserId(User $userId): self
    {
        $this->userId->removeElement($userId);
        $userId->removeEquipment($this);

        return $this;
    }

    /**
     * @return Collection<int, Exercice>
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Exercice $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
            $exercice->setRequiredEquipmentId($this);
        }

        return $this;
    }

    public function removeExercice(Exercice $exercice): self
    {
        if ($this->exercices->removeElement($exercice)) {
            // set the owning side to null (unless already changed)
            if ($exercice->getRequiredEquipmentId() === $this) {
                $exercice->setRequiredEquipmentId(null);
            }
        }

        return $this;
    }
    public function __toString(){
        return $this->equipmentName;
    }
}
