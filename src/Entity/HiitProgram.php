<?php

namespace App\Entity;

use App\Repository\HiitProgramRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HiitProgramRepository::class)
 */
class HiitProgram
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hiitName;

    /**
     * @ORM\Column(type="integer")
     */
    private $timer;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="hiitPrograms")
     */
    private $creatorId;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="favedHiitPrograms")
     */
    private $FavedByUserId;

    /**
     * @ORM\ManyToMany(targetEntity=Exercice::class,  inversedBy="hiitId")
     */
    private $exercices;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    public function __construct()
    {
        $this->FavedByUserId = new ArrayCollection();
        $this->exercices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHiitName(): ?string
    {
        return $this->hiitName;
    }

    public function setHiitName(string $hiitName): self
    {
        $this->hiitName = $hiitName;

        return $this;
    }

    public function getTimer(): ?int
    {
        return $this->timer;
    }

    public function setTimer(int $timer): self
    {
        $this->timer = $timer;

        return $this;
    }

    public function getCreatorId(): ?User
    {
        return $this->creatorId;
    }

    public function setCreatorId(?User $creatorId): self
    {
        $this->creatorId = $creatorId;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getFavedByUserId(): Collection
    {
        return $this->FavedByUserId;
    }

    public function addFavedByUserId(User $favedByUserId): self
    {
        if (!$this->FavedByUserId->contains($favedByUserId)) {
            $this->FavedByUserId[] = $favedByUserId;
            $favedByUserId->addFavedHiitProgram($this);
        }

        return $this;
    }

    public function removeFavedByUserId(User $favedByUserId): self
    {
        $this->FavedByUserId->removeElement($favedByUserId);
        $favedByUserId->removeFavedHiitProgram($this);

        return $this;
    }

    /**
     * @return Collection<int, Exercice>
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Exercice $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
            $exercice->addHiitId($this);
        }

        return $this;
    }

    public function removeExercice(Exercice $exercice): self
    {
        if ($this->exercices->removeElement($exercice)) {
            $exercice->removeHiitId($this);
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function __toString(){
        return $this->hiitName;
    }


}
