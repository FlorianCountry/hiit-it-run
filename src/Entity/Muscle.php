<?php

namespace App\Entity;

use App\Repository\MuscleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MuscleRepository::class)
 */
class Muscle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $muscleName;

    /**
     * @ORM\ManyToMany(targetEntity=MuscleGroup::class, inversedBy="muscles")
     */
    private $muscleGroupId;

    /**
     * @ORM\OneToMany(targetEntity=Exercice::class, mappedBy="firstMuscleId")
     */
    private $exercicesFirst;

    /**
     * @ORM\OneToMany(targetEntity=Exercice::class, mappedBy="secondMuscleId")
     */
    private $exercicesSecond;

    /**
     * @ORM\OneToMany(targetEntity=Exercice::class, mappedBy="thirdMuscleId")
     */
    private $exercicesThird;

    public function __construct()
    {
        $this->muscleGroupId = new ArrayCollection();
        $this->exercices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMuscleName(): ?string
    {
        return $this->muscleName;
    }

    public function setMuscleName(string $muscleName): self
    {
        $this->muscleName = $muscleName;

        return $this;
    }

    /**
     * @return Collection<int, MuscleGroup>
     */
    public function getMuscleGroupId(): Collection
    {
        return $this->muscleGroupId;
    }

    public function addMuscleGroupId(MuscleGroup $muscleGroupId): self
    {
        if (!$this->muscleGroupId->contains($muscleGroupId)) {
            $this->muscleGroupId[] = $muscleGroupId;
        }

        return $this;
    }

    public function removeMuscleGroupId(MuscleGroup $muscleGroupId): self
    {
        $this->muscleGroupId->removeElement($muscleGroupId);

        return $this;
    }

    /**
     * @return Collection<int, Exercice>
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Exercice $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
            $exercice->setFirstMuscleId($this);
        }

        return $this;
    }

    public function removeExercice(Exercice $exercice): self
    {
        if ($this->exercices->removeElement($exercice)) {
            // set the owning side to null (unless already changed)
            if ($exercice->getFirstMuscleId() === $this) {
                $exercice->setFirstMuscleId(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return $this->muscleName;
    }
}
