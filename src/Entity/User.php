<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity=Equipment::class, inversedBy="userId")
     */
    private $equipment;

    /**
     * @ORM\OneToMany(targetEntity=HiitProgram::class, mappedBy="creatorId")
     */
    private $hiitPrograms;

    /**
     * @ORM\ManyToMany(targetEntity=HiitProgram::class, mappedBy="FavedByUserId")
     */
    private $favedHiitPrograms;


    public function __construct()
    {
        $this->equipment = new ArrayCollection();
        $this->hiitPrograms = new ArrayCollection();
        $this->favedHiitPrograms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Equipment>
     */
    public function getEquipment(): Collection
    {
        return $this->equipment;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipment->contains($equipment)) {
            $this->equipment[] = $equipment;
            $equipment->addUserId($this);
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        if ($this->equipment->removeElement($equipment)) {
            $equipment->removeUserId($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, HiitProgram>
     */
    public function getHiitPrograms(): Collection
    {
        return $this->hiitPrograms;
    }

    public function addHiitProgram(HiitProgram $hiitProgram): self
    {
        if (!$this->hiitPrograms->contains($hiitProgram)) {
            $this->hiitPrograms[] = $hiitProgram;
            $hiitProgram->setCreatorId($this);
        }

        return $this;
    }

    public function removeHiitProgram(HiitProgram $hiitProgram): self
    {
        if ($this->hiitPrograms->removeElement($hiitProgram)) {
            // set the owning side to null (unless already changed)
            if ($hiitProgram->getCreatorId() === $this) {
                $hiitProgram->setCreatorId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HiitProgram>
     */
    public function getFavedHiitPrograms(): Collection
    {
        return $this->favedHiitPrograms;
    }

    public function addFavedHiitProgram(HiitProgram $favedHiitProgram): self
    {
        if (!$this->favedHiitPrograms->contains($favedHiitProgram)) {
            $this->favedHiitPrograms[] = $favedHiitProgram;
            $favedHiitProgram->addFavedByUserId($this);
        }

        return $this;
    }

    public function removeFavedHiitProgram(HiitProgram $favedHiitProgram): self
    {
        if ($this->favedHiitPrograms->removeElement($favedHiitProgram)) {
            $favedHiitProgram->removeFavedByUserId($this);
        }

        return $this;
    }
    public function __toString(){
        return $this->username;
    }

}
