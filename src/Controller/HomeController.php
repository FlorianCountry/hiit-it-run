<?php

namespace App\Controller;

use App\Data\HiitGenerator;
use App\Entity\HiitProgram;
use App\Entity\Muscle;
use App\Entity\MuscleGroup;
use App\Form\GeneratorType;
use App\Repository\ExerciceRepository;
use App\Repository\MuscleGroupRepository;
use App\Repository\MuscleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\GeneratedHiitController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index( MuscleGroupRepository $muscleGroupRepository, ExerciceRepository $exerciceRepository, Request $request ): Response
    {
        return $this->redirectToRoute('app_hiit_program_accueil', [], Response::HTTP_PERMANENTLY_REDIRECT);
    }


}
