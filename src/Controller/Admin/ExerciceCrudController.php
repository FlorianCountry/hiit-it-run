<?php

namespace App\Controller\Admin;

use App\Entity\Exercice;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ExerciceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Exercice::class;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('exerciceName', 'Nom de l\'exercice'),
            AssociationField::new('requiredEquipmentId', 'Equipement nécessaire'),
            BooleanField::new('cardioFocus', 'Focus cardio ? '),
            IntegerField::new('cardioIntensity', 'Intensité cardio (1 à 5)'),
            AssociationField::new('firstMuscleId', 'Principal muscle sollicité'),
            IntegerField::new('firstMuscleIntensity', 'Intensité musculaire (1 à 5)'),
            AssociationField::new('secondMuscleId', 'Second muscle sollicité'),
            IntegerField::new('secondMuscleIntensity', 'Intensité musculaire (1 à 5)'),
            AssociationField::new('thirdMuscleId', 'Troisième muscle sollicité'),
            IntegerField::new('thirdMuscleIntensity', 'Intensité musculaire (1 à 5)'),
            TextareaField::new('exerciceDesc', 'Description de l\'exercice')->hideOnIndex(),
            TextareaField::new('exerciceLink', 'Lien vers plus d\'infos sur l\'exercice')->hideOnIndex(),
            TextField::new('exerciceImg', 'url de l\'image')->hideOnIndex(),
        ];
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
