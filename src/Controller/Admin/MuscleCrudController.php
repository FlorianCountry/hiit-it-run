<?php

namespace App\Controller\Admin;

use App\Entity\Muscle;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MuscleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Muscle::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('muscleName', 'Nom du muscle'),
            AssociationField::new('muscleGroupId', 'Groupes musculaires'),
//            AssociationField::new('exercices', 'Liste des exercices')->hideOnForm(),
        ];
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
