<?php

namespace App\Controller\Admin;

use App\Entity\Equipment;
use App\Entity\Exercice;
use App\Entity\Muscle;
use App\Entity\MuscleGroup;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Hiititrun');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Home', 'fas fa-home', 'app_home');
        yield MenuItem::linkToDashboard('Dashboard', 'fas fa-clipboard-list');
         yield MenuItem::linkToCrud('Muscles', 'fas fa-bars', Muscle::class);
         yield MenuItem::linkToCrud('Groupes musculaires', 'fas fa-bars', MuscleGroup::class);
         yield MenuItem::linkToCrud('Equipement', 'fa fa-dumbbell', Equipment::class);
         yield MenuItem::linkToCrud('Exercices', 'fas fa-stopwatch-20', Exercice::class);
         yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-users', User::class);
    }
}
