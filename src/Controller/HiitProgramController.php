<?php

namespace App\Controller;

use App\Data\HiitGenerator;
use App\Entity\HiitProgram;
use App\Entity\User;
use App\Form\GeneratedProgramType;
use App\Form\GeneratorType;
use App\Form\HiitProgramType;
use App\Repository\ExerciceRepository;
use App\Repository\HiitProgramRepository;
use App\Repository\MuscleGroupRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hiit")
 */
class HiitProgramController extends AbstractController
{

    /**
     * @Route("/hiit", name="app_hiit_program_accueil", methods={"GET"})
     */
    public function accueil(HiitProgramRepository $hiitProgramRepository, MuscleGroupRepository $muscleGroupRepository, Request $request, ExerciceRepository $exerciceRepository, EntityManagerInterface $entityManager, UserRepository $userRepository): Response
    {
        $generator = new HiitGenerator();
        $user = $this->getUser();
        $form = $this->createForm(GeneratorType::class, $generator);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $nbExercices = ($generator->duree * 60 / 40 / 6);
            $hiitProgram = new HiitProgram();
            $hiitProgram->setHiitName("");
            $hiitProgram->setTimer($generator->duree);
            $hiitProgram->setCreatorId($this->getUser());
            $hiitProgram->setStatus(false);
            if ($generator->focusCardio == true ){
                if (is_null($user)) {
                    foreach ($exerciceRepository->getCardioFocusExercicenoEquipment($nbExercices) as $exercice) {
                        $hiitProgram->addExercice($exercice);
                    }
                }
                else{
                    foreach ($exerciceRepository->getCardioFocusExercice($nbExercices, $user) as $exercice) {
                        $hiitProgram->addExercice($exercice);
                    }
                }
            }
            else{
                if (is_null($user)) {
                    foreach ($exerciceRepository->getRandomExercicesByMuscleGroupNoEquipment($generator->groupesMusculaires, $nbExercices) as $exercice) {
                        $hiitProgram->addExercice($exercice);
                    }
                }
                else{
                    foreach ($exerciceRepository->getRandomExercicesByMuscleGroup($generator->groupesMusculaires, $nbExercices, $user) as $exercice){
                        $hiitProgram->addExercice($exercice);
                    }
                }
            }
            $hiitProgramRepository->add($hiitProgram);
            if ($generator->focusCardio == true ){
                $hiitProgram->setHiitName("HIIT Focus cardio");
            }
            else{
                $hiitProgram->setHiitName("HIIT ".$generator->groupesMusculaires);
            }

            $entityManager->persist($hiitProgram);
            $entityManager->flush();

            return $this->redirectToRoute('app_hiit_program_show', ['id' => $hiitProgram->getId()],
                Response::HTTP_SEE_OTHER
            );


        }

        return $this->render('hiit_program/accueilhiit.html.twig', [
            'form'=>$form->createView()
        ]);

    }

    /**
     * @Route("/hiit-genere/", name="app_hiit_program_genere",methods={"GET"})
     */
    public function hiitGenere(HiitProgramRepository $hiitProgramRepository, ExerciceRepository $exerciceRepository, Request $request){



        return $this->render('hiit_program/generated.html.twig', [
            'exercices' => $exerciceRepository->getRandomExercicesByMuscleGroupNoEquipment(1, 4)
        ]);

    }

    /**
     * @Route ("/{id}/fav", name="app_hiit_fav", requirements={"id": "\d+"}, methods={"GET"})
     * @param HiitProgram $hiitProgram
     */
    public function addtoFav(HiitProgram $hiitProgram, EntityManagerInterface $entityManager){
        $user = $this->getUser();
        $hiitProgram->addFavedByUserId($user);
        $entityManager->persist($hiitProgram);
        $entityManager->flush();
        $this->addFlash('alert-success', 'Programme ajouté à vos favoris');

        return $this->redirectToRoute('app_hiit_program_show', ['id' => $hiitProgram->getId()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route ("/{id}/unfav", name="app_hiit_unfav", requirements={"id": "\d+"}, methods={"GET"})
     * @param HiitProgram $hiitProgram
     */
    public function removeFromFav(HiitProgram $hiitProgram, EntityManagerInterface $entityManager){
        $user = $this->getUser();
        $hiitProgram->removeFavedByUserId($user);
        $entityManager->persist($hiitProgram);
        $entityManager->flush();
        $this->addFlash('alert-success', 'Programme ajouté à vos favoris');

        return $this->redirectToRoute('app_hiit_program_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route ("/{id}/publish", name="app_hiit_publish", requirements={"id": "\d+"}, methods={"GET"})
     * @param HiitProgram $hiitProgram
     */
    public function publish(HiitProgram $hiitProgram, EntityManagerInterface $entityManager){
        $hiitProgram->setStatus(true);
        $entityManager->persist($hiitProgram);
        $entityManager->flush();
        $this->addFlash('alert-success', 'Programme publié');

        return $this->redirectToRoute('app_hiit_program_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route ("/{id}/unpublish", name="app_hiit_unpublish", requirements={"id": "\d+"}, methods={"GET"})
     * @param HiitProgram $hiitProgram
     */
    public function unpublish(HiitProgram $hiitProgram, EntityManagerInterface $entityManager){
        $hiitProgram->setStatus(false);
        $entityManager->persist($hiitProgram);
        $entityManager->flush();
        $this->addFlash('alert-success', 'Programme publié');

        return $this->redirectToRoute('app_hiit_program_index', [], Response::HTTP_SEE_OTHER);
    }


    /**
     * @Route("/", name="app_hiit_program_index", methods={"GET"})
     */
    public function index(HiitProgramRepository $hiitProgramRepository): Response
    {
        return $this->render('hiit_program/index.html.twig', [
            'hiit_programs' => $hiitProgramRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_hiit_program_new", methods={"GET", "POST"})
     */
    public function new(Request $request, HiitProgramRepository $hiitProgramRepository): Response
    {
        $hiitProgram = new HiitProgram();
        $hiitProgram->setCreatorId($this->getUser());
        $form = $this->createForm(HiitProgramType::class, $hiitProgram);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hiitProgramRepository->add($hiitProgram);
            return $this->redirectToRoute('app_hiit_program_show', ['id' => $hiitProgram->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('hiit_program/new.html.twig', [
            'hiit_program' => $hiitProgram,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_hiit_program_show", methods={"GET"})
     */
    public function show(HiitProgram $hiitProgram, HiitProgramRepository $hiitProgramRepository): Response
    {
        $nbExercices = 0;
        foreach ($hiitProgram->getExercices() as $exercice ) {
            $nbExercices += 1;
        }
        return $this->render('hiit_program/show.html.twig', [
            'hiit_program' => $hiitProgram,
            'secondes' => $second =  $hiitProgram->getTimer()*60 ,
            'nbExercices' => $nbExercices,
            'series' => (round($second / 40 / $nbExercices))

        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_hiit_program_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, HiitProgram $hiitProgram, HiitProgramRepository $hiitProgramRepository): Response
    {
        $form = $this->createForm(HiitProgramType::class, $hiitProgram);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hiitProgramRepository->add($hiitProgram);
            return $this->redirectToRoute('app_hiit_program_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('hiit_program/edit.html.twig', [
            'hiit_program' => $hiitProgram,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_hiit_program_delete", methods={"POST"})
     */
    public function delete(Request $request, HiitProgram $hiitProgram, HiitProgramRepository $hiitProgramRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$hiitProgram->getId(), $request->request->get('_token'))) {
            $hiitProgramRepository->remove($hiitProgram);
        }

        return $this->redirectToRoute('app_hiit_program_index', [], Response::HTTP_SEE_OTHER);
    }
}
