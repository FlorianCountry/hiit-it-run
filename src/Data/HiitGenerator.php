<?php

namespace App\Data;

use Doctrine\ORM\Mapping as ORM;


class HiitGenerator
{
     /**
      * @ORM\Column (type="integer")
      */
     public $duree;

     /**
      * @ORM\Column (type="boolean")
      */
     public $focusCardio;

     /**
      * @ORM\Column  (type="string")
      */
     public $groupesMusculaires;

}