<?php

namespace App\Form;

use App\Data\HiitGenerator;
use App\Entity\HiitProgram;
use App\Entity\MuscleGroup;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeneratedProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hiitName')
            ->add('timer')
            ->add('groupesMusculaires', EntityType::class, [
                'label' => 'Groupes musculaires ciblés',
                'required' =>false,
                'class' =>MuscleGroup::class,
                'expanded' => false,
                'multiple' => false,
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => HiitProgram::class,
        ]);
    }
}
