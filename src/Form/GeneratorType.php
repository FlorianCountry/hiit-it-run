<?php

namespace App\Form;

use App\Data\HiitGenerator;
use App\Entity\MuscleGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class GeneratorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('duree', IntegerType::class, [
                'label' => 'Durée en minutes',
                'required' => true,
            ])
        ->add('focusCardio', CheckboxType::class, [
            'label' => 'Focus Cardio uniquement ? ',
            'required' =>false,
        ])
        ->add('groupesMusculaires', EntityType::class, [
            'label' => 'Groupes musculaires ciblés',
            'required' =>false,
            'class' =>MuscleGroup::class,
            'expanded' => false,
            'multiple' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'generator_class' => HiitGenerator::class,
            'method' => 'GET',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

}